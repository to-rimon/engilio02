<?php

//Authのルート
Auth::routes();

//ポートフォリオのリソースフルルート
Route::resource('/portfolio','PortfolioController');

//企業情報のリソースフルルート
Route::resource('/company','CompanyController');

//ユーザー情報のリソースフルルート
Route::resource('/user','UserController');

//マイページを表示するルート
Route::get('/mypage','MainController@showMypage');

//プロフィール編集ページを表示するルート
Route::get('/mypage/edit','MainController@showMypageEdit');

//フォロー一覧を表示するルート
Route::get('/follows','MainController@showFollows');

//ポートフォリオの登録画面を表示するルート
Route::get('/create/portfolio','MainController@showCreatePortfolio');

//該当のポートフォリオの編集画面を表示するルート
Route::get('/edit/portfolio/{id}','MainController@showEditPortfolio');

//企業情報の登録画面を表示するルート
Route::get('/create/company','MainController@showCreateCompany');

//該当の企業情報の編集画面を表示するルート
Route::get('/edit/company/{id}','MainController@showEditCompany');

//退会画面表示ルート
Route::get('/delete','MainController@showDelete');

//メッセージ一覧を表示するルート
Route::get('/message','MessageController@showMessageList');

//メッセージボードを表示するルート
Route::get('/board/{id}','MessageController@showMessageBoard');

//メッセージを登録するルート
Route::post('/board/{id}','MessageController@postMessage');

//ユーザー向けトップページを表示するルート
Route::get('/', 'MainController@showTop');

//企業向けトップページを表示するルート
Route::get('/top/company', 'MainController@showCompanyTop');

//検索画面表示ルート
Route::get('/search', 'MainController@showSearch');

//利用規約画面表示ルート
Route::get('/serviceterms', 'MainController@showServiceTerms');
