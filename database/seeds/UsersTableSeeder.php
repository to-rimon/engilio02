<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $param = [
            'name' => 'test1',
            'email' => 'abc@gmail.com',
            'password' => 'asdfgh',
            'birthday' => '2000/01/01',
        ];
        DB::table('users')->updateOrInsert($param);

        $param = [
            'name' =>  'test2',
            'email' =>  'edf@gmail.com',
            'password' => 'asdfgh',
            'birthday' => '2000/01/01',
        ];
        DB::table('users')->updateOrInsert($param);

        $param = [
            'name' => 'test3',
            'email' =>  'ghi@gmail.com',
            'password' => 'asdfgh',
            'birthday' => '2000/01/01',
        ];
        DB::table('users')->updateOrInsert($param);
    }
}
