<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('board_id');
            $table->unsignedBigInteger('send_user_id');
            $table->unsignedBigInteger('receive_user_id');
            $table->text('message');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('board_id')->references('id')->on('boards');
            $table->foreign('send_user_id')->references('id')->on('users');
            $table->foreign('receive_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
