<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->string('img')->nullable();
            $table->string('job')->nullable();
            $table->unsignedBigInteger('job_period')->nullable();
            $table->text('career')->nullable();
            $table->text('self_introduction')->nullable();
            $table->string('order1')->nullable();
            $table->unsignedBigInteger('price1')->nullable();
            $table->string('order2')->nullable();
            $table->unsignedBigInteger('price2')->nullable();
            $table->string('order3')->nullable();
            $table->unsignedBigInteger('price3')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
