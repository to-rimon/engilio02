<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('person_user_id');
            $table->bigInteger('attr')->default(1);
            $table->string('name');
            $table->string('email')->unique();
            $table->string('logo_img')->nullable();
            $table->string('title');
            $table->string('img')->nullable();
            $table->text('overview')->nullable();
            $table->text('business')->nullable();
            $table->text('character')->nullable();
            $table->text('use_language')->nullable();
            $table->text('recruit_hope')->nullable();
            $table->boolean('release_flg')->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('person_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
