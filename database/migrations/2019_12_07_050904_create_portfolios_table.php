<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->boolean('release_flg')->default(false);
            $table->string('title');
            $table->string('img')->nullable();
            $table->text('overview')->nullable();
            $table->text('target')->nullable();
            $table->text('purpose')->nullable();
            $table->text('concept')->nullable();
            $table->text('appeal')->nullable();
            $table->text('language')->nullable();;
            $table->string('url')->nullable();;
            $table->string('release')->nullable();;
            $table->string('create_time')->nullable();;
            $table->unsignedBigInteger('cost')->nullable();;
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
