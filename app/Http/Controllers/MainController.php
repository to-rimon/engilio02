<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    //トップページのアクセスアクション
    public function showTop()
    {
        return view('top');
    }
    //企業向けトップページのアクセスアクション
    public function showCompanyTop()
    {
        return view('top_company');
    }
    //検索ページへのアクセスアクション
    public function showSearch()
    {
        return view('search');
    }
    //マイページへのアクセスアクション
    public function showMypage()
    {
        return view('users.mypage');
    }
    //プロフ更新ページへのアクセスアクション
    public function showMypageEdit()
    {
        return view('users.edit');
    }
    //フォロー一覧ページへのアクセスアクション
    public function showFollows()
    {
        return view('users.follows');
    }
    //ポートフォリオ作成ページへのアクセスアクション
    public function showCreatePortfolio()
    {
        return view('portfolios.create');
    }
    //ポートフォリオ編集ページへのアクセスアクション
    public function showEditPortfolio()
    {
        return view('portfolios.edit');
    }
    //企業情報登録ページへのアクセスアクション
    public function showCreateCompany()
    {
        return view('companies.create');
    }
    //企業情報編集ページへのアクセスアクション
    public function showEditCompany()
    {
        return view('companies.edit');
    }
    //退会ページへのアクセスアクション
    public function showDelete()
    {
        return view('users.delete');
    }
    //利用規約ページへのアクセスアクション
    public function showServiceTerms()
    {
        return view('serviceterms');
    }
}
